// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Sound/SoundCue.h"
#include "Components/AudioComponent.h"
#include "Components/CapsuleComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "TimerManager.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Controllers/FighterPlayerController.h"
#include "Controllers/FighterAIController.h"
#include "GameModes/GazeGameMode.h"
#include "Math/UnrealMathUtility.h"
#include "GameModes/GazeGameState.h"
#include "Kismet/GameplayStatics.h"
#include "AIController.h"
#include "Fighter.generated.h"

class AGazeGameState;

UCLASS()
class GAZE_API AFighter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;

public:
	// Sets default values for this character's properties
	AFighter();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Laser)
		TSubclassOf<class AActor> ParticleSystem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Laser)
		UParticleSystem* HitParticleSystem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Laser)
		USoundCue * LaserNoise;

	UAudioComponent * LaserAudioComponent;

	UFUNCTION(BlueprintCallable, Category = Combat)
		void TriggerAttack();

	UFUNCTION(BlueprintCallable, Category = Combat)
		void EndAttack();

	UFUNCTION(BlueprintCallable, Category = Combat)
		void MoveBeam();

	UFUNCTION()
		void OnHitEnemy(AActor* EnemyHit, FVector HitPoint, FHitResult HitResult);

	UFUNCTION()
		void ReceiveDamage(float DamageAmount);

	UFUNCTION()
		void IncrementElimCount();

	UFUNCTION()
		void RemoveFromHitList(AFighter* Hit);

	UFUNCTION()
		void RemoveFromDeadList(AFighter* Dead);

	UFUNCTION(BlueprintCallable, Category = Stats)
		void DepleteMana(float ManaDelta);

	UFUNCTION(BlueprintCallable, Category = Stats)
		void RegenMana(float ManaDelta);

	UFUNCTION(BlueprintCallable, Category = Stats)
		void SetMana(float NewMana);

	UFUNCTION(BlueprintCallable, Category = Stats)
		float GetMana();

	UFUNCTION(BlueprintCallable, Category = Stats)
		void SetHealth(float NewHealth);

	UFUNCTION(BlueprintCallable, Category = Stats)
		float GetHealth();

	UFUNCTION(BlueprintCallable, Category = Camera)
		UCameraComponent* GetFollowCamera();

	UFUNCTION(BlueprintCallable, Category = Combat)
		bool GetAttackTriggered();

	UFUNCTION(BlueprintCallable, Category = Combat)
		float GetBeamRange();

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Combat)
	UForceFeedbackEffect* HitFeedbackEffect;

	float CurrentSpeed;

	float CurrentRotationRate;

	bool IsMoving;

	bool IsAccelerating;

	float SpeedWhenStopping;

	float SpeedRequiredForLeap;

	float LateralSpeed;

private:

	float Mana = 100;

	float BeamRange = 1500.f;

	float Health = 100;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void PostInitializeComponents() override;

	bool AttackTriggered = false;
	UParticleSystemComponent* Laser;
	AActor* LaserActor;

	UFUNCTION()
		void ReactHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		UCameraComponent* PlayerCamera;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		USpringArmComponent* PlayerSpringArm;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat)
		TArray<AFighter *> HitFighters;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat)
		TArray<AFighter*> DeadFighters;

	USceneComponent* HeadLocation;

public:	

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
