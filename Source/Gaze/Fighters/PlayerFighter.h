// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Fighter.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "UI/PauseMenuWidget.h"
#include "PlayerFighter.generated.h"

class UUserWidget;
class AHUD;

UCLASS(config = Game)
class APlayerFighter : public AFighter
{
	GENERATED_BODY()

	// Public variables
	public:
		/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
			float BaseTurnRate;

		/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
			float BaseLookUpRate;

	// Public functions
	public:
		APlayerFighter();

		UFUNCTION(BlueprintCallable, Category = UI)
			void ShowScoreboard();
		
		UFUNCTION(BlueprintCallable, Category = UI)
			void HideScoreboard();

		UFUNCTION(BlueprintCallable, Category = UI)
			void SetScoreboardWidget(UScoreboardWidget* Scoreboard);

		UFUNCTION(BlueprintCallable, Category = UI)
			void SetIngameWidget(UInGameWidget* InGame);

		UFUNCTION(BlueprintCallable, Category = UI)
			void SetPauseMenuWidget(UPauseMenuWidget* PauseMenu);

		UFUNCTION(BlueprintCallable, Category = UI)
			void TogglePauseMenu();

	// Protected functions
	protected:
		/** Called for forwards/backward input */
		void MoveForward(float Value);

		/** Called for side to side input */
		void MoveRight(float Value);

		/**
		 * Called via input to turn at a given rate.
		 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
		 */
		void TurnAtRate(float Rate);

		/**
		 * Called via input to turn look up/down at a given rate.
		 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
		 */
		void LookUpAtRate(float Rate);

		virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Private variables
	private:
		float Mana;

		UScoreboardWidget* Scoreboard;
		TSubclassOf<UScoreboardWidget> ScoreboardClass;

		UPauseMenuWidget* PauseMenu;
		TSubclassOf<UPauseMenuWidget> PauseMenuClass;

		UInGameWidget* InGame;
		TSubclassOf<UInGameWidget> InGameClass;

	// Private functions
	private:
		virtual void BeginPlay();
		virtual void Tick(float DeltaTime) override;
};

