// Fill out your copyright notice in the Description page of Project Settings.


#include "Fighter.h"
#include "AbilitySystemComponent.h"
#include "GameplayTagContainer.h"
#include "Animation/AnimInstance.h"
#include "Abilities/GameplayAbilityTypes.h"
//#include "Abilities/FighterAttributeSet.h"
#include "AbilitySystemBlueprintLibrary.h"

// Sets default values
AFighter::AFighter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//// Define attribute set for monsters.
	//AttributeSet = CreateDefaultSubobject<UFighterAttributeSet>(TEXT("AttributeSet"));

	HeadLocation = CreateDefaultSubobject<USceneComponent>(TEXT("HeadLocation"));
	HeadLocation->SetRelativeLocation(FVector(0.f, 0.f, 0.f));

	LaserAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("LaserAudioComp"));

	// Bind ReactHit to be called whenever OnActorHit is broadcast.
	FScriptDelegate Delegate;
	Delegate.BindUFunction(this, "ReactHit");
	this->OnActorHit.AddUnique(Delegate);

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 0.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
}

// Called when the game starts or when spawned
void AFighter::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AFighter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AFighter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AFighter::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	if (LaserAudioComponent)
	{
		LaserAudioComponent->SetSound(LaserNoise);
	}
	
}

void AFighter::ReactHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor->ActorHasTag(FName("Player.Laser")) && Cast<AFighter>(OtherActor->GetAttachParentActor()) != this)
	{	
		// If it's a player who's hit, play the haptic feedback
		if (this->ActorHasTag(FName("Controlled")))
			Cast<APlayerController>(this->GetController())->ClientPlayForceFeedback(HitFeedbackEffect, false, NAME_None);

		this->ReceiveDamage(5.f);
		if (this->GetHealth() == 0)
		{
			if (Cast<AFighter>(OtherActor->GetAttachParentActor()))
			{
				Cast<AFighter>(OtherActor->GetAttachParentActor())->IncrementElimCount();
			}

			this->GetCharacterMovement()->DisableMovement();
			this->GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
			this->GetMesh()->SetAllBodiesSimulatePhysics(true);
			this->GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);


			AGazeGameMode* Gaze = Cast<AGazeGameMode>(this->GetWorld()->GetAuthGameMode());
			if (this->ActorHasTag(FName("AI")))
			{
				// Has to happen before InitRespawn because the controller will detach from pawn
				Cast<AGazeGameState>(this->GetWorld()->GetGameState())->AddDeath(Cast<AFighterAIController>(this->GetController())->GetPlayerID());
				Gaze->InitRespawn(FString("AI"), 0, this->GetController());
			}
			else if (this->ActorHasTag(FName("Controlled")))
			{
				// Has to happen before InitRespawn because the controller will detach from pawn
				Cast<AGazeGameState>(this->GetWorld()->GetGameState())->AddDeath(Cast<AFighterPlayerController>(this->GetController())->GetPlayerID());
				Gaze->InitRespawn(FString("Controlled"), 0, this->GetController());
			}
		}
	}
}


void AFighter::TriggerAttack()
{
	if (this->GetMana() != 0.f)
	{
		// Create a line trace to decide the initial starting and ending point of the beam.
		FVector Start = this->GetMesh()->GetSocketLocation("headSocket");
		FVector End = (GetFollowCamera()->GetForwardVector() *	BeamRange) + Start;
		FHitResult HitResult;
		FCollisionQueryParams Params;
		this->GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECollisionChannel::ECC_Pawn, Params);

		// Spawning an actor so that we can pass that into "ActorOnHit" for whomever it collides with.
		// Once we've spawned the actor, we can get the ParticleSystemComponent from it that we added.
		this->LaserActor = this->GetWorld()->SpawnActor<AActor>(ParticleSystem, this->GetActorLocation(), FRotator(0, 0, 0));
		this->LaserActor->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform, FName("headSocket"));
		if (this->LaserActor->GetAttachParentActor())
			UE_LOG(LogTemp, Warning, TEXT("Attach parent actor"))
		this->Laser = Cast<UParticleSystemComponent>(LaserActor->GetComponentByClass(UParticleSystemComponent::StaticClass()));

		// Adding a tag to the laser actor
		this->LaserActor->Tags.Add(FName("Player.Laser"));

		if (this->Laser)
		{
			this->Laser->SetBeamSourcePoint(0, Start, 0);
			if (HitResult.Actor.Get())
			{
				End = HitResult.ImpactPoint;
				this->Laser->SetBeamEndPoint(0, End);
				OnHitEnemy(HitResult.Actor.Get(), End, HitResult);
				AttackTriggered = true;
			}
			else
			{
				this->Laser->SetBeamEndPoint(0, End);
			}
		}
		else
		{
			TriggerAttack();
		}

		// Everything else is done, so play the audio :)
		LaserAudioComponent->Play();
	}

	if (this->GetMana() == 0.f && LaserActor)
	{
		Laser->DestroyComponent();
		LaserAudioComponent->Stop();
	}
}

void AFighter::EndAttack()
{
	AttackTriggered = false;
	if (Laser && LaserAudioComponent)
	{
		Laser->DestroyComponent();
		LaserAudioComponent->Stop();
	}
}

void AFighter::MoveBeam()
{
	if (this->GetMana() != 0.f)
	{
		FVector Start = this->GetMesh()->GetSocketLocation("headSocket");
		FVector End = (GetFollowCamera()->GetForwardVector() * BeamRange) + Start;
		FHitResult HitResult;
		FCollisionQueryParams Params;
		Params.AddIgnoredActor(this);


		this->GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECollisionChannel::ECC_PhysicsBody, Params);
		if (this->Laser)
		{
			this->Laser->SetBeamSourcePoint(0, Start, 0);
			if (HitResult.Actor.Get())
			{
				End = HitResult.ImpactPoint;
				this->Laser->SetBeamEndPoint(0, End);
				OnHitEnemy(HitResult.Actor.Get(), End, HitResult);
			}
			else
			{
				this->Laser->SetBeamEndPoint(0, End);
			}
		}
		else
		{
			TriggerAttack();
		}
	}

	if (this->GetMana() == 0.f && LaserActor)
	{
		Laser->DestroyComponent();
		LaserAudioComponent->Stop();
	}
}

void AFighter::OnHitEnemy(AActor* EnemyHit, FVector HitPoint,  FHitResult HitResult)
{
	if (EnemyHit && EnemyHit != this)
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitParticleSystem, HitPoint);
	// Broadcast OnActorHit if it's a player
	if (EnemyHit && !HitFighters.Contains(EnemyHit) && EnemyHit->ActorHasTag(FName("Character")) && Cast<AFighter>(EnemyHit)->GetHealth() != 0)
	{
		HitFighters.Add(Cast<AFighter>(EnemyHit));
		EnemyHit->OnActorHit.Broadcast(EnemyHit, this->LaserActor, HitPoint, HitResult);

		FTimerDelegate TimerDel;
		FTimerHandle TimerHandle;

		//Binding the function with specific variables
		TimerDel.BindUFunction(this, FName("RemoveFromHitList"), Cast<AFighter>(HitResult.Actor.Get()));
		//Calling MyUsefulFunction after 5 seconds without looping
		GetWorldTimerManager().SetTimer(TimerHandle, TimerDel, .01f, false);
	}
}

void AFighter::ReceiveDamage(float DamageAmount)
{
	this->SetHealth(FMath::Clamp(this->GetHealth() - DamageAmount, 0.f, 100.f));
}

void AFighter::IncrementElimCount()
{
	if (this->ActorHasTag(FName("AI")))
	{
		Cast<AGazeGameState>(GetWorld()->GetGameState())->AddElim(Cast<AFighterAIController>(this->GetController())->GetPlayerID());
	}
	else if (this->ActorHasTag(FName("Controlled")))
	{
		Cast<AGazeGameState>(GetWorld()->GetGameState())->AddElim(Cast<AFighterPlayerController>(this->GetController())->GetPlayerID());
	}
}

/*
 * This is called after a specific amount of time to ensure that players are able to be hit again.
 */
void AFighter::RemoveFromHitList(AFighter* Hit)
{
	int numRemoved = HitFighters.Remove(Hit);
}

void AFighter::RemoveFromDeadList(AFighter* Dead)
{
	int numRemoved = DeadFighters.Remove(Dead);
}

void AFighter::DepleteMana(float ManaDelta)
{
	this->SetMana(FMath::Clamp(this->GetMana() - ManaDelta, 0.f, 100.f));
}

void AFighter::RegenMana(float ManaDelta)
{
	this->SetMana(FMath::Clamp(this->GetMana() + ManaDelta, 0.f, 100.f));
}

void AFighter::SetMana(float NewMana)
{
	this->Mana = NewMana;
}

float AFighter::GetMana()
{
	return this->Mana;
}

void AFighter::SetHealth(float NewHealth)
{
	this->Health = NewHealth;
}

float AFighter::GetHealth()
{
	return this->Health;
}

UCameraComponent* AFighter::GetFollowCamera()
{
	if (!this->FollowCamera)
	{
		this->FollowCamera = Cast<UCameraComponent>(this->GetDefaultSubobjectByName(FName("FollowCamera")));
	}
	return this->FollowCamera;
}

bool AFighter::GetAttackTriggered()
{
	return this->AttackTriggered;
}

float AFighter::GetBeamRange()
{
	return this->BeamRange;
}
