// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "PlayerFighter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "AbilitySystemComponent.h"
#include "GameplayTagContainer.h"
#include "Abilities/GameplayAbilityTypes.h"
#include "AbilitySystemBlueprintLibrary.h"
#include "GameFramework/SpringArmComponent.h"

//////////////////////////////////////////////////////////////////////////
// APlayerFighter

APlayerFighter::APlayerFighter()
{
	// Set default pawn class for our character(s)
	static ConstructorHelpers::FClassFinder<UScoreboardWidget> ScoreboardWidgetBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/UI/ScoreboardWidget_BP"));
	if (ScoreboardWidgetBPClass.Class != NULL)
	{
		ScoreboardClass = ScoreboardWidgetBPClass.Class;
	}

	// Set default pawn class for our character(s)
	static ConstructorHelpers::FClassFinder<UInGameWidget> InGameWidgetBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/UI/InGameWidget_BP"));
	if (InGameWidgetBPClass.Class != NULL)
	{
		InGameClass = InGameWidgetBPClass.Class;
	}

	// Set default pawn class for our character(s)
	static ConstructorHelpers::FClassFinder<UPauseMenuWidget> PauseMenuWidgetBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/UI/PauseMenuWidget_BP"));
	if (PauseMenuWidgetBPClass.Class != NULL)
	{
		PauseMenuClass = PauseMenuWidgetBPClass.Class;
	}

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

void APlayerFighter::BeginPlay()
{
	Super::BeginPlay();
}

void APlayerFighter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FTimerDelegate TimerDel;
	FTimerHandle RegenTimerHandle;
	if (AttackTriggered)
	{
		MoveBeam();

		// We're going to delay the depletion of mana for 0.1 seconds... Helps with the UI.
		FTimerDelegate TimerDel;
		FTimerHandle TimerHandle;

		int StaminaDelta = 1;

		//Binding the function with specific variables
		TimerDel.BindUFunction(this, FName("DepleteMana"), 0.5f);
		//Calling MyUsefulFunction after 5 seconds without looping
		GetWorldTimerManager().SetTimer(TimerHandle, TimerDel, .1f, false);
	}
	else if (!AttackTriggered)
	{
		//Binding the function with specific variables
		TimerDel.BindUFunction(this, FName("RegenMana"), 0.5f);
		//Calling MyUsefulFunction after 5 seconds without looping
		GetWorldTimerManager().SetTimer(RegenTimerHandle, TimerDel, .1f, false);
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void APlayerFighter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &APlayerFighter::TriggerAttack);
	PlayerInputComponent->BindAction("Attack", IE_Released, this, &APlayerFighter::EndAttack);

	PlayerInputComponent->BindAction("ToggleScoreboard", IE_Pressed, this, &APlayerFighter::ShowScoreboard);
	PlayerInputComponent->BindAction("ToggleScoreboard", IE_Released, this, &APlayerFighter::HideScoreboard);

	PlayerInputComponent->BindAction("TogglePauseMenu", IE_Pressed, this, &APlayerFighter::TogglePauseMenu);

	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerFighter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerFighter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &APlayerFighter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &APlayerFighter::LookUpAtRate);

}

void APlayerFighter::ShowScoreboard()
{
	Scoreboard->SetVisibility(ESlateVisibility::Visible);
}

void APlayerFighter::HideScoreboard()
{
	Scoreboard->SetVisibility(ESlateVisibility::Hidden);
}

void APlayerFighter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void APlayerFighter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void APlayerFighter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void APlayerFighter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void APlayerFighter::SetScoreboardWidget(UScoreboardWidget* Scoreboard)
{
	this->Scoreboard = Scoreboard;
}

void APlayerFighter::SetIngameWidget(UInGameWidget* InGame)
{
	this->InGame = InGame;
}

void APlayerFighter::SetPauseMenuWidget(UPauseMenuWidget* PauseMenu)
{
	this->PauseMenu = PauseMenu;
}

void APlayerFighter::TogglePauseMenu()
{
	if (PauseMenu->Visibility == ESlateVisibility::Hidden)
	{
		PauseMenu->SetVisibility(ESlateVisibility::Visible);
		Cast<APlayerController>(this->GetController())->SetInputMode(FInputModeUIOnly());
		Cast<APlayerController>(this->GetController())->bShowMouseCursor = true;
	}
}