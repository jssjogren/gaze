// Fill out your copyright notice in the Description page of Project Settings.


#include "FighterPlayerController.h"

AFighterPlayerController::AFighterPlayerController()
{
}

void AFighterPlayerController::BeginPlay()
{
	Super::BeginPlay();

	// We are trying to uniquely identify each AI Controller. To do this, we're using the GameState to keep track of available IDs
	// Once we get the next "settable" ID we set it for this controller. Pawns are temporary, controllers are forever.
	this->SetPlayerID(Cast<AGazeGameState>(this->GetWorld()->GetGameState())->GetCurrentID());

	// Now we increment the next ID in the game state.
	Cast<AGazeGameState>(this->GetWorld()->GetGameState())->IncrementCurrentID();

	this->SetFighterColor(FString("Blue"));
	Cast<AGazeGameState>(this->GetWorld()->GetGameState())->SetFighterColor(this->GetPlayerID(), FString("Blue"));
	UE_LOG(LogTemp, Warning, TEXT("Player COlor in GS: %s"), *Cast<AGazeGameState>(this->GetWorld()->GetGameState())->GetFighterColor(this->GetPlayerID()))
}

// Set whether or not this controller's pawn is dead.
void AFighterPlayerController::SetIsDead(bool IsDead)
{
	this->IsDead = IsDead;
}

// Get whether or not this controller's pawn is dead.
bool AFighterPlayerController::GetIsDead()
{
	return this->IsDead;
}

// Get this controller's unique ID.
int AFighterPlayerController::GetPlayerID()
{

	return this->PlayerID;
}

// Set this controller's unique ID.
void AFighterPlayerController::SetPlayerID(int NewPlayerID)
{
	this->PlayerID = NewPlayerID;
}

FString AFighterPlayerController::GetFighterColor()
{
	return this->FighterColor;
}

void AFighterPlayerController::SetFighterColor(FString NewFighterColor)
{
	this->FighterColor = NewFighterColor;
}