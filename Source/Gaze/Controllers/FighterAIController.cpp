// Fill out your copyright notice in the Description page of Project Settings.


#include "FighterAIController.h"

AFighterAIController::AFighterAIController()
{
}

void AFighterAIController::BeginPlay()
{
	Super::BeginPlay();

	//this->UseBlackboard(BlackboardData, Blackboard);
	// We want the enemies (right now) to attack the single player
	//if (Blackboard)
	//	Blackboard->SetValueAsObject(FName("Enemy"), GetWorld()->GetFirstPlayerController()->GetPawn());

	// Set the color of the controller/player
	/*this->SetPlayerColor(FString("Pink"));*/
	/*Cast<AGazeGameState>(this->GetWorld()->GetGameState())->SetPlayerColor(this->GetPlayerID(), FString("Pink"));*/

	// Now we increment the next ID in the game state.
	if (Cast<AGazeGameState>(this->GetWorld()->GetGameState()))
	{
		// We are trying to uniquely identify each AI Controller. To do this, we're using the GameState to keep track of available IDs
		// Once we get the next "settable" ID we set it for this controller. Pawns are temporary, controllers are forever.
		this->SetPlayerID(Cast<AGazeGameState>(this->GetWorld()->GetGameState())->GetCurrentID());

		// Now we increment the next ID in the game state.
		Cast<AGazeGameState>(this->GetWorld()->GetGameState())->IncrementCurrentID();

		this->SetFighterColor(FString("Red"));
		Cast<AGazeGameState>(this->GetWorld()->GetGameState())->SetFighterColor(this->GetPlayerID(), FString("Red"));
		UE_LOG(LogTemp, Warning, TEXT("AI COlor in GS: %s"), *Cast<AGazeGameState>(this->GetWorld()->GetGameState())->GetFighterColor(this->GetPlayerID()))
	}
}

void AFighterAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Set whether or not this controller's pawn is dead.
void AFighterAIController::SetIsDead(bool IsDead)
{
	this->IsDead = IsDead;
}

// Get whether or not this controller's pawn is dead.
bool AFighterAIController::GetIsDead()
{
	return this->IsDead;
}

// Get this controller's unique ID.
int AFighterAIController::GetPlayerID()
{

	return this->PlayerID;
}

// Set this controller's unique ID.
void AFighterAIController::SetPlayerID(int NewPlayerID)
{
	this->PlayerID = NewPlayerID;
}

FString AFighterAIController::GetFighterColor()
{
	return this->FighterColor;
}

void AFighterAIController::SetFighterColor(FString NewFighterColor)
{
	this->FighterColor = NewFighterColor;
}