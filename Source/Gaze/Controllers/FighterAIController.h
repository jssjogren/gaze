// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "GameFramework/PlayerController.h"
#include "GameModes/GazeGameState.h"
#include "UObject/ConstructorHelpers.h"
#include "FighterAIController.generated.h"

/**
 * 
 */
UCLASS()
class GAZE_API AFighterAIController : public AAIController
{
	GENERATED_BODY()

	// Private variables
	private:
		// The controller's unique ID.
		int PlayerID;
		// The controller's unique color.
		FString FighterColor;
		// Whether the controller's pawn is dead.
		bool IsDead;

	// Private functions
	private:
		// Called when the game starts or when spawned
		virtual void BeginPlay() override;

		// Called every frame
		virtual void Tick(float DeltaTime) override;

	// Public variables
	public:

	// Public functions
	public:

		AFighterAIController();

		UFUNCTION(BlueprintCallable, Category = Stats)
			void SetIsDead(bool IsDead);

		UFUNCTION(BlueprintCallable, Category = Stats)
			bool GetIsDead();

		UFUNCTION(Category = PlayerInfo)
			int GetPlayerID();

		UFUNCTION(Category = PlayerInfo)
			void SetPlayerID(int NewPlayerID);

		UFUNCTION(Category = PlayerInfo)
			FString GetFighterColor();

		UFUNCTION(Category = PlayerInfo)
			void SetFighterColor(FString NewFighterColor);
};
