// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GameModes/GazeGameState.h"
#include "UI/ScoreboardWidget.h"
#include "UObject/ConstructorHelpers.h"
#include "UI/InGameWidget.h"
#include "FighterPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class GAZE_API AFighterPlayerController : public APlayerController
{
	GENERATED_BODY()

	// Private variables
	private:
		// The controller's unique ID.
		int PlayerID;
		// The controller's unique color.
		FString FighterColor;
		// Whether the controller's pawn is dead.
		bool IsDead;
	
	// Private functions
	private:
		AFighterPlayerController();

		// Called when the game starts or when spawned
		virtual void BeginPlay() override;

	// Public functions
	public:

		UFUNCTION(BlueprintCallable, Category = Stats)
			void SetIsDead(bool IsDead);

		UFUNCTION(BlueprintCallable, Category = Stats)
			bool GetIsDead();

		UFUNCTION(Category = PlayerInfo)
			int GetPlayerID();

		UFUNCTION(Category = PlayerInfo)
			void SetPlayerID(int NewPlayerID);

		UFUNCTION(Category = PlayerInfo)
			FString GetFighterColor();

		UFUNCTION(Category = PlayerInfo)
			void SetFighterColor(FString NewFighterColor);
};
