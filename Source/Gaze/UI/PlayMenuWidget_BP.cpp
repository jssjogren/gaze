// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayMenuWidget_BP.h"

void UPlayMenuWidget_BP::StartGame()
{
	UGameplayStatics::OpenLevel(GetWorld(), FName("/Game/ThirdPersonCPP/Maps/ThirdPersonExampleMap"), false, FString());
	AGazeGameMode* GameMode = Cast<AGazeGameMode>(GetWorld()->GetAuthGameMode());
	Cast<UGazeGameInstance>(GetWorld()->GetGameInstance())->SetSelectedNumAIs(AICount);
	Cast<UGazeGameInstance>(GetWorld()->GetGameInstance())->SetSelectedNumPlayers(PlayerCount);
	RemoveFromViewport();
}

void UPlayMenuWidget_BP::RemovePlayers(UTextBlock *Players, UTextBlock *AIs)
{
	UE_LOG(LogTemp, Warning, TEXT("Remove Players"))
	if (PlayerCount > 1)
	{
		PlayerCount--;

		Players->SetText(FText::FromString(FString::FromInt(PlayerCount)));
	}
}

void UPlayMenuWidget_BP::AddPlayers(UTextBlock *Players, UTextBlock *AIs)
{
	UE_LOG(LogTemp, Warning, TEXT("Add Players %d %d"), PlayerCount, AICount)
	UE_LOG(LogTemp, Warning, TEXT("Text %s"), *Players->Text.ToString())
	if (PlayerCount < 4 && PlayerCount + AICount < 4)
	{
		PlayerCount++;

		Players->SetText(FText::FromString(FString::FromInt(PlayerCount)));
	}
	else if (PlayerCount < 4 && PlayerCount + AICount >= 4)
	{
		PlayerCount++;
		AICount--;

		Players->SetText(FText::FromString(FString::FromInt(PlayerCount)));
		AIs->SetText(FText::FromString(FString::FromInt(AICount)));
	}
}

void UPlayMenuWidget_BP::RemoveAIs(UTextBlock *Players, UTextBlock *AIs)
{
	UE_LOG(LogTemp, Warning, TEXT("Remove AIs"))
	if (AICount > 0 && PlayerCount != 1)
	{
		AICount--;

		AIs->SetText(FText::FromString(FString::FromInt(AICount)));
	}
	else if (AICount > 0 && PlayerCount == 1)
	{
		PlayerCount++;
		AICount--;

		Players->SetText(FText::FromString(FString::FromInt(PlayerCount)));
		AIs->SetText(FText::FromString(FString::FromInt(AICount)));
	}
}

void UPlayMenuWidget_BP::AddAIs(UTextBlock *Players, UTextBlock *AIs)
{
	UE_LOG(LogTemp, Warning, TEXT("Add AIs"))
	if (AICount < 3 && AICount + PlayerCount < 4)
	{
		AICount++;

		AIs->SetText(FText::FromString(FString::FromInt(AICount)));
	}
	else if (AICount < 3 && AICount + PlayerCount >= 4)
	{
		PlayerCount--;
		AICount++;

		Players->SetText(FText::FromString(FString::FromInt(PlayerCount)));
		AIs->SetText(FText::FromString(FString::FromInt(AICount)));
	}
}
