// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "Components/TextBlock.h"
#include "GameFramework/GameModeBase.h"
#include "GameModes/GazeGameMode.h"
#include "GameModes/GazeGameInstance.h"
#include "PlayMenuWidget_BP.generated.h"

/**
 * 
 */
UCLASS()
class GAZE_API UPlayMenuWidget_BP : public UUserWidget
{
	GENERATED_BODY()

	UFUNCTION(BlueprintCallable, Category = "Game")
	void StartGame();

	UFUNCTION(BlueprintCallable, Category = "Players")
	void RemovePlayers(UTextBlock* Players, UTextBlock* AIs);
	UFUNCTION(BlueprintCallable, Category = "Players")
	void AddPlayers(UTextBlock* Players, UTextBlock* AIs);

	UFUNCTION(BlueprintCallable, Category = "AIs")
	void RemoveAIs(UTextBlock* Players, UTextBlock* AIs);
	UFUNCTION(BlueprintCallable, Category = "AIs")
	void AddAIs(UTextBlock* Players, UTextBlock* AIs);

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Players")
		int PlayerCount = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AIs")
		int AICount = 3;
};
