// Fill out your copyright notice in the Description page of Project Settings.


#include "ScoreboardWidget.h"

TArray<int> UScoreboardWidget::SortTMap(TMap<int, int> MapToSort)
{
	MapToSort.ValueSort([](int32 A, int32 B) {
		return B < A; // sort keys in reverse
	});

	TArray<int> LineIDs;
	for (auto& Elem : MapToSort)
	{
		LineIDs.Add(Elem.Key);
	}

	return LineIDs;
}