// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameModes/GazeGameState.h"
#include "Blueprint/UserWidget.h"
#include "ScoreboardWidget.generated.h"

/**
 * 
 */
UCLASS()
class GAZE_API UScoreboardWidget : public UUserWidget
{
	GENERATED_BODY()

	UFUNCTION(BlueprintCallable, Category = Sorting)
		TArray<int> SortTMap(TMap<int, int> MapToSort);
};
