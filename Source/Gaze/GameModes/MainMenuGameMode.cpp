// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuGameMode.h"
#include <Runtime\Engine\Classes\Kismet\GameplayStatics.h>

AMainMenuGameMode::AMainMenuGameMode()
{
	static ConstructorHelpers::FClassFinder<AHUD> MainMenuHUDBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ui/MainMenuHUD_BP"));
	if (MainMenuHUDBPClass.Class != NULL)
	{
		UE_LOG(LogTemp, Warning, TEXT("Class"))
		HUDClass = MainMenuHUDBPClass.Class;
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("Not class"))
}

void AMainMenuGameMode::StartGame()
{
	UGameplayStatics::OpenLevel(GetWorld(), FName("World'/Game/ThirdPersonCPP/Blueprints/ui/MainMenu.MainMenu'"), false, FString());
}

void AMainMenuGameMode::EndGame()
{
	UKismetSystemLibrary::QuitGame(GetWorld(), nullptr, EQuitPreference::Quit, true);
}