// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "GazeGameState.generated.h"

class AFighter;
/**
 * 
 */
UCLASS()
class GAZE_API AGazeGameState : public AGameStateBase
{
	GENERATED_BODY()

	AGazeGameState();
	
	private:
		class AFighter * Fighter;
		// TMap<FighterID, NumElims>
		TMap<int, int> FighterElims;
		// TMap<FighterID, NumDeaths>
		TMap<int, int> FighterDeaths;
		// TMap<FighterID, FighterColor>
		TMap<int, FString> FighterColors;
		// Total number of players/AI
		int CurrentID = 0;

	public:
		UFUNCTION(BlueprintCallable, Category = GameStats)
			void AddElim(int FighterToIncrement);

		UFUNCTION(BlueprintCallable, Category = GameStats)
			void RemoveElim(int FighterToDecrement);

		UFUNCTION(BlueprintCallable, Category = GameStats)
			int GetNumElims(int FighterToGrab);

		UFUNCTION(BlueprintCallable, Category = GameStats)
			void AddDeath(int FighterToIncrement);

		UFUNCTION(BlueprintCallable, Category = GameStats)
			void RemoveDeath(int FighterToDecrement);

		UFUNCTION(BlueprintCallable, Category = GameStats)
			int GetNumDeaths(int FighterToGrab);

		UFUNCTION(BlueprintCallable, Category = GameStats)
			void IncrementCurrentID();

		UFUNCTION(BlueprintCallable, Category = GameStats)
			int GetCurrentID();

		UFUNCTION(BlueprintCallable, Category = GameStats)
			void SetFighterColor(int FighterToSet, FString Color);

		UFUNCTION(BlueprintCallable, Category = GameStats)
			FString GetFighterColor(int FighterToGrab);
};
