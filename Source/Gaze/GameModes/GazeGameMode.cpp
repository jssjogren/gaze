// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "GazeGameMode.h"
#include "Fighters/PlayerFighter.h"
#include "GameFramework/PlayerStart.h"
#include "UObject/ConstructorHelpers.h"
#include <Runtime\Engine\Classes\Kismet\GameplayStatics.h>
#include <Runtime\AIModule\Classes\AIController.h>

AGazeGameMode::AGazeGameMode()
{
	// Set default pawn class for our character(s)
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/PlayerFighter_BP"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		PlayerPawnClass = PlayerPawnBPClass.Class;
	}

	// Set default pawn class of our AIs
	static ConstructorHelpers::FClassFinder<APawn> AIPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/EnemyFighter_BP"));
	if (AIPawnBPClass.Class != NULL)
	{
		AIPawnClass = AIPawnBPClass.Class;
	}

	// Set default player controller class for our character(s)
	static ConstructorHelpers::FClassFinder<AFighterPlayerController> PlayerControllerBPClass(TEXT("AFighterPlayerController'/Game/ThirdPersonCPP/Blueprints/Controllers/BP_PlayerController'"));
	if (PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<AHUD> InGameHUDBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ui/InGameHUD_BP"));
	if (InGameHUDBPClass.Class != NULL)
	{
			HUDClass = InGameHUDBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<AGameStateBase> GazeGameStateClass(TEXT("/Game/ThirdPersonCPP/Blueprints/Gamemodes/BP_GazeGameState"));
	if (GazeGameStateClass.Class != NULL)
	{
			GameStateClass = GazeGameStateClass.Class;
	}

	// We don't want a default pawn, we're going to manually spawn them.
	DefaultPawnClass = NULL;

}

void AGazeGameMode::BeginPlay()
{
	int SelectedPlayers = Cast<UGazeGameInstance>(GetWorld()->GetGameInstance())->GetSelectedNumPlayers();
	int SelectedAIs = Cast<UGazeGameInstance>(GetWorld()->GetGameInstance())->GetSelectedNumAIs();

	TSubclassOf<APlayerStart> playerStartClass;
	playerStartClass = APlayerStart::StaticClass();
	TArray<AActor*> foundPlayerStarts;
	// fetch all actors of the class 'PlayerStart'
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), playerStartClass, foundPlayerStarts);

	// Loop through all player starts and create a player there.
	for (int i = 0; i < SelectedPlayers; i++)
	{
		// Making sure our PlayerPawn class is there
		if (PlayerPawnClass != NULL)
		{
			// Creates a new PlayerController with the ID of our loop index (i)
			UGameplayStatics::CreatePlayer(GetWorld(), i, true);

			// Spawn a new Actor of the class AFighter at the correct spawn location, get the correct controller, and possess the character.
			// Once this is done, store the unique character in our array to be used later on.
			AFighter* NewActor = GetWorld()->SpawnActor<AFighter>(PlayerPawnClass, foundPlayerStarts[0]->GetActorLocation(), FRotator(0,0,0));
			AActor* StartToRemove = foundPlayerStarts[0];
			foundPlayerStarts.Remove(StartToRemove);
			APlayerController * Controller = UGameplayStatics::GetPlayerController(GetWorld(), i);
			Controller->Possess(NewActor);
			NewActor->Tags.Add(FName("Controlled"));
			NewActor->Tags.Add(FName("Character"));
			Players.AddUnique(NewActor);
			/*NewActor->GetController()->SetPlayerID(i);*/
		}
	}

	for (int j = 0; j < SelectedAIs; j++)
	{
		if (AIPawnClass != NULL)
		{
			// Spawn a new Actor of the class AFighter at the correct spawn location, get the correct controller, and possess the character.
			// Once this is done, store the unique character in our array to be used later on.
			AFighter* NewActor = GetWorld()->SpawnActor<AFighter>(AIPawnClass, foundPlayerStarts[0]->GetActorLocation(), FRotator(0, 0, 0));
			AActor* StartToRemove = foundPlayerStarts[0];
			foundPlayerStarts.Remove(StartToRemove);
			NewActor->SpawnDefaultController();
			if (NewActor->GetController())
				UE_LOG(LogTemp, Warning, TEXT("Controller OK"))
			else
				UE_LOG(LogTemp, Warning, TEXT("Controller not OK"))

			NewActor->Tags.Add(FName("AI"));
			NewActor->Tags.Add(FName("Character"));
			AIs.AddUnique(NewActor);

			/*NewActor->GetController()->SetPlayerID(i);*/
		}
	}
}

void AGazeGameMode::InitRespawn(FString CharacterType, int CharacterID, AController* CharacterController)
{
	if (CharacterController)
	{
		if (CharacterType.Equals("AI"))
		{
			Cast<AFighterAIController>(CharacterController)->SetIsDead(true);
			AIs.Remove(CharacterController->GetPawn());
		}
		else
		{
			Cast<AFighterPlayerController>(CharacterController)->SetIsDead(true);
			Players.Remove(CharacterController->GetPawn());
		}
		
		CharacterController->GetPawn()->SetLifeSpan(7);
		CharacterController->UnPossess();

		FTimerDelegate TimerDel;
		FTimerHandle TimerHandle;

		// Params to pass into function once it ticks
		int32 MyInt = 10;
		float MyFloat = 20.f;

		//Binding the function with specific variables
		TimerDel.BindUFunction(this, FName("Respawn"), CharacterType, CharacterController);
		//Calling MyUsefulFunction after 5 seconds without looping
		GetWorldTimerManager().SetTimer(TimerHandle, TimerDel, 7.f, false);
	}
}

void AGazeGameMode::Respawn(FString CharacterType, AController* CharacterController) 
{
	TSubclassOf<APlayerStart> playerStartClass;
	playerStartClass = APlayerStart::StaticClass();
	TArray<AActor*> foundPlayerStarts;
	// fetch all actors of the class 'PlayerStart'
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), playerStartClass, foundPlayerStarts);
	int RandomPlayerStart = rand() % foundPlayerStarts.Num();

	if (CharacterType.Equals("AI"))
	{
		UE_LOG(LogTemp, Warning, TEXT("Respawn AI"))
		AFighter* NewActor = GetWorld()->SpawnActor<AFighter>(AIPawnClass, foundPlayerStarts[RandomPlayerStart]->GetActorLocation(), FRotator(0, 0, 0));
		CharacterController->Possess(NewActor);
		//Cast<AFighterAIController>(CharacterController)->RunBehaviorTree(Cast<AFighterAIController>(CharacterController)->BehaviorTree);
		AIs.AddUnique(NewActor);
		NewActor->Tags.Add(FName("AI"));
		NewActor->Tags.Add(FName("Character"));
		Cast<AFighterAIController>(CharacterController)->SetIsDead(false);
	}
	else if (CharacterType.Equals("Controlled"))
	{
		AFighter* NewActor = GetWorld()->SpawnActor<AFighter>(PlayerPawnClass, foundPlayerStarts[RandomPlayerStart]->GetActorLocation(), FRotator(0, 0, 0));
		CharacterController->Possess(NewActor);
		Players.AddUnique(NewActor);
		NewActor->Tags.Add(FName("Controlled"));
		NewActor->Tags.Add(FName("Character"));
		Cast<AFighterPlayerController>(CharacterController)->SetIsDead(false);
	}
}
