// Fill out your copyright notice in the Description page of Project Settings.

#include "GazeGameState.h"

AGazeGameState::AGazeGameState()
{
	UE_LOG(LogTemp, Warning, TEXT("Game state"));
}

void AGazeGameState::AddElim(int FighterToIncrement)
{
	if (this->FighterElims.Contains(FighterToIncrement))
	{
		int OldElimCount = *this->FighterElims.Find(FighterToIncrement);
		OldElimCount++;
		this->FighterElims.Add(FighterToIncrement, OldElimCount);
	}
	else if (!this->FighterElims.Contains(FighterToIncrement))
	{
		this->FighterElims.Add(FighterToIncrement, 1);
	}
	UE_LOG(LogTemp, Warning, TEXT("Elims in Game State: %d"), *this->FighterElims.Find(FighterToIncrement));
}

void AGazeGameState::RemoveElim(int FighterToDecrement)
{
	if (this->FighterElims.Contains(FighterToDecrement))
	{
		int OldElimCount = *this->FighterElims.Find(FighterToDecrement);
		OldElimCount--;
		this->FighterElims.Add(FighterToDecrement, OldElimCount);
	}
	UE_LOG(LogTemp, Warning, TEXT("Elims in Game State: %d"), *this->FighterElims.Find(FighterToDecrement));
}

int AGazeGameState::GetNumElims(int FighterToGrab)
{
	if (!this->FighterElims.Find(FighterToGrab))
		this->FighterElims.Add(FighterToGrab, 0);
	return *this->FighterElims.Find(FighterToGrab);
}

void AGazeGameState::AddDeath(int FighterToIncrement)
{
	if (this->FighterDeaths.Contains(FighterToIncrement))
	{
		int OldDeathCount = *this->FighterDeaths.Find(FighterToIncrement);
		OldDeathCount++;
		this->FighterDeaths.Add(FighterToIncrement, OldDeathCount);
	}
	else if (!this->FighterDeaths.Contains(FighterToIncrement))
	{
		this->FighterDeaths.Add(FighterToIncrement, 1);
	}
	UE_LOG(LogTemp, Warning, TEXT("Deaths in Game State: %d"), *this->FighterDeaths.Find(FighterToIncrement));
}

void AGazeGameState::RemoveDeath(int FighterToDecrement)
{
	if (this->FighterDeaths.Contains(FighterToDecrement))
	{
		int OldDeathCount = *this->FighterDeaths.Find(FighterToDecrement);
		OldDeathCount--;
		this->FighterDeaths.Add(FighterToDecrement, OldDeathCount);
	}
}

int AGazeGameState::GetNumDeaths(int FighterToGrab)
{
	if (!this->FighterDeaths.Find(FighterToGrab))
		this->FighterDeaths.Add(FighterToGrab, 0);
	return *this->FighterDeaths.Find(FighterToGrab);
}

void AGazeGameState::IncrementCurrentID()
{
	CurrentID++;
}

int AGazeGameState::GetCurrentID()
{
	return CurrentID;
}

void AGazeGameState::SetFighterColor(int FighterToSet, FString Color)
{
	this->FighterColors.Add(FighterToSet, Color);
}

FString AGazeGameState::GetFighterColor(int FighterToGrab)
{
	if (!this->FighterColors.Find(FighterToGrab))
		this->FighterColors.Add(FighterToGrab, FString("White"));
	return *this->FighterColors.Find(FighterToGrab);
}
