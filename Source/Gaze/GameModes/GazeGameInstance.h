// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "GazeGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class GAZE_API UGazeGameInstance : public UGameInstance
{
	GENERATED_BODY()

private:
	UPROPERTY()
		int NumPlayers = 1;

	UPROPERTY()
		int NumAIs = 3;
public:
	UFUNCTION()
		void SetSelectedNumPlayers(int NewNumPlayers);
	UFUNCTION()
		void SetSelectedNumAIs(int NewNumAIs);
	UFUNCTION()
		int GetSelectedNumPlayers();
	UFUNCTION()
		int GetSelectedNumAIs();
};
