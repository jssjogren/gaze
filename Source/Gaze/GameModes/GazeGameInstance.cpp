// Fill out your copyright notice in the Description page of Project Settings.


#include "GazeGameInstance.h"

void UGazeGameInstance::SetSelectedNumPlayers(int NewNumPlayers)
{
	this->NumPlayers = NewNumPlayers;
}

void UGazeGameInstance::SetSelectedNumAIs(int NewNumAIs)
{
	this->NumAIs = NewNumAIs;
}

int UGazeGameInstance::GetSelectedNumPlayers()
{
	return this->NumPlayers;
}

int UGazeGameInstance::GetSelectedNumAIs()
{
	return this->NumAIs;
}