// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GameFramework/HUD.h"
#include "GameFramework/GameStateBase.h"
#include "Controllers/FighterPlayerController.h"
#include "GameModes/GazeGameInstance.h"
#include "GazeGameMode.generated.h"

UCLASS(minimalapi)
class AGazeGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGazeGameMode();
	virtual void BeginPlay();

	UFUNCTION()
		void InitRespawn(FString CharacterType, int CharacterID, AController * CharacterController);

	UFUNCTION()
		void Respawn(FString CharacterType, AController* CharacterController);

private:
	TSubclassOf<APawn> PlayerPawnClass;
	TSubclassOf<APawn> AIPawnClass;
	TSubclassOf<AFighterPlayerController> PlayerControllerBPClass;

	TArray<APawn*> Players;
	TArray<APawn*> AIs;
};



